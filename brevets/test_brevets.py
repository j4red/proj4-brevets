"""
Functionality tests for use with the nosetests module. Tests basic functionality
of my implementation of the brevets calculator.

Author: Jared Knofczynski
Last modified: Nov. 8, 2020
"""
import arrow, nose
from acp_times import *

# ---- Testing Globals -----
utc = arrow.utcnow().floor('minute')
tomorrow = utc.shift(days=1)
utc_iso = utc.isoformat()
durations = {
    200: 13.5,
    300: 20.0,
    400: 27.0,
    600: 40.0,
    1000: 75.0
}


def test_standard_open ():
    """ Test standard open-time functionality for specific controls. """
    assert open_time(34, 200, utc_iso) == utc.shift(hours=1).isoformat()
    assert open_time(200, 200, utc_iso) == utc.shift(hours=5, minutes=53).isoformat()

    assert open_time(34, 1000, utc_iso) == utc.shift(hours=1).isoformat() # still takes 1hr for first 34 km
    assert open_time(1000, 1000, utc_iso) == utc.shift(hours=33, minutes=5).isoformat()


def test_standard_close ():
    """ Test standard close-time functionality for single control brevets. """
    for i in durations:
        assert close_time(i, i, utc_iso) == utc.shift(hours=durations[i]).isoformat()


def test_intermediate_control ():
    """ Test standard functionality for intermediate controls.
        A control at 200km closes after 13:30 in 200km brevet, but closes after
        13:20 in any other length. This function tests that that functionality
        is implemented properly. """
    assert close_time(200, 200, utc_iso) == utc.shift(hours=durations[200]).isoformat()
    assert close_time(200, 1000, utc_iso) == utc.shift(hours=13, minutes=20).isoformat()


def test_slightly_over ():
    """ Test that standard functionality works properly for controls slightly beyond
        the max distance, e.g. a control at 209KM for a 200KM brevet. """
    assert open_time(209, 200, utc_iso) == utc.shift(hours=5, minutes=53).isoformat()
    assert close_time(205, 200, utc_iso) == utc.shift(hours=durations[200]).isoformat()


def test_french ():
    """ Test standard functionality for French variation.
        French variation indicates controls within the first 60KM will have
        slightly relaxed closing times given by 20KM/hr + 1hr. Past the 60KM
        mark, closing times return to normal. """
    assert close_time(0, 200, utc_iso) == utc.shift(hours=1).isoformat()
    assert close_time(20, 200, utc_iso) == utc.shift(hours=2).isoformat()
    assert close_time(60, 200, utc_iso) == utc.shift(hours=4).isoformat()
    assert close_time(200, 200, utc_iso) == utc.shift(hours=durations[200]).isoformat()


def test_negative_distance ():
    """ Test functionality if KM <= 0. (Negative distances should not be
        possible, so results are constrained to non-negative values.) """
    assert open_time(-1, 600, utc_iso) == utc.isoformat()
    assert close_time(-999, 200, utc_iso) == utc.shift(hours=1).isoformat()


def test_max_duration ():
    """ Tests that race cannot exceed specified duration for brevet length. """
    for i in durations:
        assert close_time(99999, i, utc_iso) == utc.shift(hours=durations[i]).isoformat()
