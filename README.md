# CIS 322 Project 4: Brevet Time Calculator with AJAX and Flask
Jared Knofczynski, jknofczy@uoregon.edu

## AJAX and Flask reimplementation

This program is a reimplementation of the RUSA Control Time Calculator (https://rusa.org/octime_acp.html) using Flask and AJAX, rather than PERL. Unlike the RUSA calculator, this implementation updates automatically and does not require the form to be submitted each time before viewing the results.

## The Front End - Using this calculator

To use this calculator, run `./run.sh` in the `brevets` directory. Afterwards, navigate to `localhost:5000` in your web browser. From there, select the brevet distance from the dropdown menu, followed by the start date and time for the brevet. The default start time is the current day at 8:00 AM.

Now you may enter the distances of each control point in either the 'Miles' or 'Km' columns on the left. After entering each distance, the open and close times for that control point will be automatically calculated and the fields to the right will be populated. Controls can be adjusted and the open/closing times will be updated automatically.

To ensure proper usage, place your control points in ascending order in either the miles or kilometers columns. Failure to do so will result in a warning in the 'Notes' column.

Similarly, if two controls fall on the same distance or one exceeds the specified brevet duration by more than 20%, a warning will occur. A warning will also occur if the control distance exceeds 1000km, as that is the maximum ACP-permitted brevet distance

## The Back End - Calculating control times

The rules for calculating control times in this program are based on those found at https://rusa.org/pages/acp-brevet-control-times-calculator and https://rusa.org/pages/rulesForRiders. This implementation also uses the French variation discussed in the 'Oddities' section in the first link. The algorithm is as follows:

 | Control Location (km) | Min. Speed (km/hr) | Max. Speed (km/hr)	|
 | :-------------: | :-------------: | :-------------:				|
 | 0 - 200  	   | 15  			 | 34							|
 | 200 - 400       | 15  			 | 32							|
 | 400 - 600       | 15  			 | 30							|
 | 600 - 1000      | 11.428  		 | 28							|
 | 1000 - 1300     | 13.333  		 | 26							|

* The distance of each control point (rounded to the nearest KM) is used in calculating the opening and closing times of said control point. The opening and closing delays for a specific control from the starting time are given by `control location / max speed` for that category.

	* A control at 170km would open 170/34 = 5 hours after the race begins, and would close 170/15 = 11.33 = 11 hours and 20 minutes after the starting time.

	* The times for controls in higher categories are given by adding the durations of their smaller parts. For example, a control at 500km in a 600km brevet would open at (200 / 34) + (200 / 32) + (100 / 30) = 15.46 = 15 hours and 28 minutes after the starting time.

* All times are rounded to the nearest minute.

* The French variation indicates that the maximum time limit for a control in the first 60KM is based on 20 KM/hr + 1 hour. After the first 60KM, the standard formula applies.

	* A control point at the beginning of the race (0 KM) will open immediately, and in the French variation, will close after (0 / 20) + 1HR = 1 hour.


* The maximum duration of the brevet depends on its length and is given by the following table.

|  Brevet Length  |    Duration		|
| :-------------: | :-------------: |
| 200 KM		  |	  13.5 hours  	|
| 300 KM		  |	  20.0 hours  	|
| 400 KM		  |	  27.0 hours  	|
| 600 KM		  |	  40.0 hours  	|
| 1000 KM		  |	  75.0 hours  	|


* As this calculator is intended for official ACP brevets only, only 200, 300, 400, 600, and 1000 KM brevets are accounted for.

## Testing

A suite of nose test cases is provided in the `brevets/test_brevets.py` file. To run these tests, navigate to the `brevets` directory and run `nosetests`.

These tests have been chosen to verify that the implementation of this control calculator aligns with the official ACP rules and that it obtains the same results as the original calculator at https://rusa.org/octime_acp.html.

## Contact information
Author: Jared Knofczynski, jknofczy@uoregon.edu  
Program designed for CIS 322 at the University of Oregon in Fall 2020.  
